# Desafio de Infra Equipe SIGA

O foco desse desafio é avaliar sua habilidade de automatizar a execução simultânea dos códigos de frontend e backend com comunicação entre eles.

## Como começar
Primeiro você irá dar "fork" nesse projeto (para isso é necessário criar uma conta no GitLab). O botão de "fork" fica no canto superior direito, após clicar não esqueça de deixar a "Visibility level" do projeto como privado. Após o "fork" você irá trabalhar nesse repositório criado.

## Objetivo
No projeto temos 2 pastas, backend e frontend. O projeto de backend é uma api simples em spring boot e o do frontend um app simples em nodejs que faz uma requisição para o backend quando acessado. Quando acessado o frontend se tudo ocorrer bem irá aparecer uma mensagem no navegador "Funcionou", caso algo dê errado a mensagem será "Nao funcionou".

O objetivo principal é fazer com que o usuário possa executar somente 1 comando e já ter acessível o frontend com a comunicação com o backend funcionando.

## Implementação
Sua implementação deverá estar no mesmo repositório que foi feito o "fork". Esse README que você está lendo agora deverá ser substituído pelo seu, que terá as instruções e informações para a execução da sua implementação. Incluindo possíveis dependências de programas que devem estar previamente instalados, informações sobre a sua implementação e instruções para a execução.

Para atingir o objetivo existem diversas maneiras, nós queremos que você implemente usando:
- [docker](https://docs.docker.com/get-started/)
- [docker-compose](https://docs.docker.com/compose/)

Será um diferencial se você utilizar/implementar:
- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [CI/CD](https://docs.gitlab.com/ee/ci/) (preferencialmente o do próprio GitLab)
- DNS para acessar o serviço no lugar de "http://localhost:3000" ou apenas redirecionar para a porta 80 sendo assim acessível digitando apenas "http://localhost". Ultilize [nginx](https://www.nginx.com/resources/wiki/start/#) para o redirecionamento de porta e no caso da utilização do minikube usar o [ingress-ningx](https://kubernetes.github.io/ingress-nginx/).

OBS: Não necessariamente é preciso utilizar todas as tecnologias ao mesmo tempo

Sinta-se livre caso queira/precise mudar o código e/ou arquivos do backend e frontend, mantendo a mesma lógica.

Caso tenha outra ideia ou se sinta mais confortável com outra solução você pode implementar no lugar do que foi citado acima, lembrando sempre que o resultado final deverá que ser o mesmo: O usuário executar apenas 1 comando para tudo funcionar. E deixar tudo explicado no README.

## Entrega
O prazo de entrega será até 23:59 do dia 09/05

Quando terminar todo seu desafio adicione o usuário @ltellesfl como membro do seu projeto privado para ser avaliado.

## Dúvidas
Caso tenha alguma dúvida em relação ao desafio abra uma [issue](https://gitlab.com/ltellesfl/desafio-infra/-/issues/new) nesse projeto público. Sua dúvida pode ser a mesma de outros.

Boa sorte :)
